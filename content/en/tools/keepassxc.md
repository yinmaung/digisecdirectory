---
title: KeePassXC
website: https://keepassxc.org/
cover: /files/keepassxc.jpg
tags:
  - Documentation & Data Management
categories:
  - Digital Security Tools
  - Password Manager
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T17:55:27.353Z
---
KeePassXC is an open-source secure password manager for Linux, macOS, and Windows.