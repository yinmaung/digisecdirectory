---
title: Digital Safety Curricula for South and Southeast Asia Region
# Verify that the language is supported before adding its ISO 639-1 code here. without the country code, i.e. ms instead of ms_MY.
languages:
  - en
website: https://engagemedia.org/2023/digital-safety-curricula-south-southeast-asia/
credits: Text by EngageMedia.
flags:
  - PDF Only
categories:
  - Digital Security Guides
date: 2023-11-20T10:49:17.797Z
---
EngageMedia's Digital Safety Curriculum, developed for South and Southeast Asia under the [Greater Internet Freedom project](https://engagemedia.org/projects/greater-internet-freedom/), is a valuable resource that equips civil society organisations (CSOs), media outlets, and individuals with essential knowledge to navigate the evolving digital security landscape.
