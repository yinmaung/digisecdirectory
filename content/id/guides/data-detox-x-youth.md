---
title: Data Detox x Youth
# Verify that the language is supported before adding its ISO 639-1 code here. without the country code, i.e. ms instead of ms_MY.
languages:
  - en
website: https://datadetoxkit.org/en/families/datadetox-x-youth/
credits: Text by Tactical Tech.
categories:
  - Panduan Keamanan Digital
date: 2023-01-25T10:16:30.255Z
---
Data Detox x Youth merupakan buku aktivitas untuk membantu anak-anak muda menguasai teknologi mereka. Seperangkat alat interaktif ini mendorong anak muda untuk memikirkan aspek kehidupan digital mereka yang lain, dari profil media sosial hingga kata sandi mereka, dengan aktivitas sederhana untuk belajar dan bermain.

Buku kegiatan ini terdiri dari empat bagian:

* Privasi Digital, yang berfokus pada pengurangan jejak data dan pemahaman pemrofilan online;
* Keamanan Digital, dengan petunjuk tentang bagaimana membuat kata sandi yang kuat dan aman;
* Kesehatan Digital, yang berhubungan dengan sifat adiktif dari smartphone;
* dan Misinformasi, sebuah panduan tentang cara mengonsumsi dan membagikan information secara online.