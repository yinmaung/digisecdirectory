---
title: Safe Sisters Guide
# Pastikan bahwa bahasa yang dimaksud sudah didukung sebelum menambahkan kode ISO 639-1 nya di sini. tanpa kode negara, mis. ms dan bukannya ms_MY.
languages:
  - en
website: https://protocol.openbriefing.org/
credits: Text by Safe Sisters.
flags:
  - Hanya PDF
categories:
  - Panduan Keamanan Digital
date: 2023-01-25T10:53:14.951Z
---
Safe Sisters Guide dimaksudkan untuk membantu saudari-saudari kita mempelajari tentang masalah yang mungkin kita temui di ineternet (seperti foto yang dicuri atau disebarkan tanpa izin, virus, dan penipuan), bagaimana cara membuat keputusan yang terinformasi setiap hari untuk melindungi diri kita sendiri, dan menjadikan internet ruang yang aman bagi diri sendiri, keluarga kita, dan semua wanita!