---
title: Riseup Surel
cover: /files/riseup.jpg
website: https://riseup.net/email
credits: Text by Khairil Zhafri/EngageMedia.
flags:
  - Freemium
  - Invite Only
tags:
  - Komunikasi & Pesan
categories:
  - Alat Keamanan Digital
  - Enkripsi OpenPGP
  - Perutean Onion
  - Surel
date: 2023-01-19T10:38:37.852Z
---
Riseup Surel adalah layanan surel aman dari aktivis untuk aktivis. Fitur keamanannya mencakup enkripsi penuh, penyimpanan surel yang terenkripsi, penjaluran Onion, dan lainnya.