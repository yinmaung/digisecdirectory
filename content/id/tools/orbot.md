---
title: Orbot
website: https://guardianproject.info/apps/org.torproject.android/
cover: /files/orbot.jpg
tags:
  - Penghindaran & Anonimitas
categories:
  - Alat Keamanan Digital
  - Perutean Onion
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T09:51:12.326Z
---
Orbot adalah aplikasi proxy untuk perangkat Android yang mengalihkan lalu lintas internet melalui jaringan Tor untuk melindungi privasi dan identitas penggunanya secara daring.