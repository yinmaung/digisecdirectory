---
title: Tor Browser
website: https://www.torproject.org/
cover: /files/torbrowser.jpg
tags:
  - Penghindaran & Anonimitas
categories:
  - Alat Keamanan Digital
  - Perutean Onion
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T09:57:54.810Z
---
Tor Browser adalah peramban internet privat terbuka untuk Windows, macOS, Linux, dan Android yang melindungi pengguna dari pemantauan, pengawasan, dan penyensoran.