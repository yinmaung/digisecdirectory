---
title: Etherpad
cover: /files/etherpad.jpg
website: https://etherpad.org/
credits: Text by Khairil Zhafri/EngageMedia.
tags:
  - Aplikasi Web
  - Hosting Mandiri
  - Penelusuran Internet & Produktivitas
categories:
  - Alat Keamanan Digital
date: 2023-01-19T21:34:39.091Z
---
Etherpad adalah alat pencatatan kolaboratif terbuka.

> [Riseup Pad](https://pad.riseup.net/) memperbolehkan kita untuk kolaborasi penyuntingan online dengan menggunakan layanan Etherpad. Riseup tidak menyimpan alamat IP. Pad akan secara otomatis hancur setelah 60 hari tanpa aktivitas.\
> – [Kebersihan Digital 101: Bagaimana menerapkan keamanan dan keselamatan digital
](https://engagemedia.org/2022/kesehatan-digital-101-tetap-sehat-melawan-virus-online/?lang=id)