---
title: Standard Notes
cover: /files/standard-notes.jpg
website: https://standardnotes.com/
credits: Text by Khairil/EngageMedia.
flags:
  - Freemium
tags:
  - Hosting Mandiri
  - Aplikasi Web
  - Lintas Platform
  - Dokumentasi & Manajemen Data
categories:
  - Alat Keamanan Digital
date: 2023-01-27T23:27:21.695Z
---
S﻿tandard Notes adalah aplikasi pencatatan dengan enkripsi ujung-ke-ujung, keamanan tingkat lanjut, dan kontrol privasi.