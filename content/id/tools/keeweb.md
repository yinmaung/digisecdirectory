---
title: KeeWeb
website: https://keeweb.info/
cover: /files/keeweb.jpg
tags:
  - Dokumentasi & Manajemen Data
  - Aplikasi Web
  - Hosting Mandiri
categories:
  - Alat Keamanan Digital
  - Pengelola Kata Sandi
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T17:43:57.715Z
---
KeeWeb adalah aplikasi pengelola kata sandi yang terdapat di KeePass. Aplikasi ini tersedia sebagai aplikasi web yang bekerja dengan peramban internet modern di perangkat komputer dan seluler.