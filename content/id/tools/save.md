---
title: Save
website: https://open-archive.org/save
cover: /files/save.jpg
tags:
  - Dokumentasi & Manajemen Data
categories:
  - Alat Keamanan Digital
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T18:00:50.203Z
---
Save buatan OpenArchive adalah aplikasi dokumentasi terbuka yang aman untuk Android dan iOS yang dapat membantu pengguna menjaga, melindungi, mengautentikasi, dan memperbaiki pekerjaan mereka.