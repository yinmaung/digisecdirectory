---
title: Outline VPN
cover: /files/outline.jpg
website: https://getoutline.org/
credits: Text by Khairil Zhafri/EngageMedia.
tags:
  - Hosting Mandiri
  - Penghindaran & Anonimitas
  - Lintas Platform
categories:
  - Alat Keamanan Digital
  - Jaringan Pribadi Virtual
date: 2023-01-19T20:13:34.694Z
---
O﻿utline mempermudah pengguna untuk mengakses internet menggunakan jaringan privat virtual (VPN). Dengan Outline Manager pengguna dapat beraktivitas di server VPN sendiri di platform awan dan Outline Client menghubungkan perangkat ke server yang ada.