---
draft: false
title: Nextcloud
website: https://nextcloud.com/
cover: /files/nextcloud.jpg
tags:
  - Hosting Mandiri
  - Dokumentasi & Manajemen Data
  - Lintas Platform
categories: 
  - Alat Keamanan Digital
  - Penyimpanan File
  - Sinkronisasi File
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-22T09:04:42.901Z
---
N﻿extcloud adalah rangkaian aplikasi kolaborasi terbuka dengan penyimpanan berkas, sinkronisasi, dan pembagian yang aman.