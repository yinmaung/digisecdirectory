---
title: DuckDuckGo
cover: /files/duckduckgo.jpg
website: https://duckduckgo.com/
credits: Naskah oleh Khairil/EngageMedia.
tags:
  - Penelusuran Internet & Produktivitas
categories:
  - Alat Keamanan Digital
date: 2023-01-29T19:51:42.320Z
---
D﻿uckDuckGo adalah mesin pencarian internet yang fokus untuk privasi.