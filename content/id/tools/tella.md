---
title: Tella
website: https://tella-app.org/
cover: /files/tella.jpg
tags:
  - Dokumentasi & Manajemen Data
categories:
  - Alat Keamanan Digital
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T18:07:09.169Z
---
Tella adalah aplikasi seluler dokumentasi dan pelaporan terbuka yang aman yang tersedia di perangkat Android dan iOS yang mengenkripsi dan menyembunyikan data tersimpan pada perangkat.