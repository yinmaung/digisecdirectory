---
title: KeePassDX
website: https://www.keepassdx.com/
cover: /files/keepassdx.jpg
tags:
  - Dokumentasi & Manajemen Data
categories:
  - Alat Keamanan Digital
  - Pengelola Kata Sandi
credits: Naskah oleh Khairil Zhafri/EngageMedia.
date: 2023-01-19T17:42:23.612Z
---
KeePassDX adalah pengelola kata sandi yang aman dengan sumber-terbuka untuk perangkat seluler Android.