---
title: OnlyOffice
cover: /files/onlyoffice.jpg
website: https://www.onlyoffice.com/
credits: Naskah oleh Khairil/EngageMedia.
flags:
  - Freemium
tags:
  - Hosting mandiri
  - Aplikasi web
  - Penelusuran Internet & Produktivitas
  - Lintas Platform
categories:
  - Alat Keamanan Digital
date: 2023-02-04T21:37:24.103Z
---
O﻿nlyOffice is adalah kelengkapan kantor pengedit online untuk dokumen, lembar kerja, dan presentasi naskah.