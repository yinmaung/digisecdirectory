---
title: Psiphon
website: https://psiphon.ca/
cover: /files/psiphon.png
tags:
  - Penghindaran & Anonimitas
categories:
  - Alat Keamanan Digital
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-19T07:24:24.913Z
---
Psiphon (Android, iOS, Windows, Mac) adalah alat terbuka yang dapat menggagalkan penyensoran internet yang menggunakan kombinasi komunikasi dan teknologi pengelabuan yang aman sehingga pengguna dapat mengakses konten yang diblokir atau disensor.