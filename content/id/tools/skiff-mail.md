---
title: Skiff Mail
website: https://skiff.com/mail
cover: /files/skiff-mail.jpg
flags:
  - Freemium
tags:
  - Komunikasi & Pesan
  - Aplikasi Web
categories:
  - Alat Keamanan Digital
  - Surel
credits: Text by Skiff.
date: 2023-01-19T10:40:05.818Z
---
Skiff Mail adalah surel terenkripsi ujung-ke-ujung yang melindungi kotak masuk Anda dan memberi Anda kuasa untuk bebas berkomunikasi.