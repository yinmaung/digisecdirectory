---
title: EngageMedia
cover: /files/engagemedia.jpg
# Verify that the language is supported before adding its ISO 639-1 code here. without the country code, i.e. ms instead of ms_MY.
languages:
  - en
  - ms
  - fil
  - id
  - my
  - th
website: https://engagemedia.org
credits: Text by EngageMedia
categories:
  - Jaringan Keamanan Digital
  - Penanggap Krisis Keamanan Digital
  - Pelatihan Keamanan Digital
date: 2023-01-30T08:20:29.849Z
---
EngageMedia adalah organisasi nirlaba yang mempromosikan hak-hak digital, teknologi yang terbuka dan aman, dan film dokumenter masalah sosial. Kami memperlengkapi masyarakat umum dengan pelatihan dan aktivitas pendidikan lainnya, membuat panduan, penelitian, dan sumber daya lainnya. Kami mengembangkan, melokalkan, terlibat dengan platform, mempromosikan perangkat lunak terbuka dan aman, serta mendukung pengetahuan dan budaya yang terbuka.