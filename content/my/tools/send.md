---
draft: false
title: Send
website: https://gitlab.com/timvisee/send
cover: /files/send.jpg
tags:
  - Self-hosting
  - Web App
  - Documentation & Data Management
categories: 
  - Digital Security Tools
  - File Sharing
credits: Text by Khairil Zhafri/EngageMedia.
date: 2023-01-22T15:50:08.622Z
---
Send is a simple, private file-sharing tool with end-to-end encryption.